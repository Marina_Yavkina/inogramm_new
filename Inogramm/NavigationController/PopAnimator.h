//
//  PopAnimator.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 03/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PopAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@end
