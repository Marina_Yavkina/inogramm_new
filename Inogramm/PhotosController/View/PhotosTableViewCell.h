//
//  PhotosTableViewCell.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Photo;

@interface PhotosTableViewCell : UITableViewCell

+ (NSString *)reuseIdentifier;
+ (CGFloat)heightForItem:(id)item widht:(CGFloat)widht;

- (instancetype)init;
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;

- (void)setItem:(Photo *)item;

@end
