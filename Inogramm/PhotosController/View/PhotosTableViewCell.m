//
//  PhotosTableViewCell.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "PhotosTableViewCell.h"
#import "Photo.h"

#import "InstagrammService.h"

static CGFloat const PhotosTableViewCellContainerMargin = 8.0f;
static CGFloat const PhotosTableViewCellContentMargin = 8.0f;
static CGFloat const PhotosTableViewCellContentOffset = 8.0f;

@interface PhotosTableViewCell ()

@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) UIImageView *photoImageView;

@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UILabel *likesLabel;

@property (nonatomic, assign) UIEdgeInsets containerInsets;
@property (nonatomic, assign) UIEdgeInsets contentInsets;

@property (nonatomic, strong) NSString *imageDownloadTaskUUID;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation PhotosTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}

+ (CGFloat)heightForItem:(Photo *)item widht:(CGFloat)widht {
    CGFloat actualWidht = widht - (PhotosTableViewCellContainerMargin * 2.0f + PhotosTableViewCellContentMargin * 2.0f);
    
    CGFloat height = PhotosTableViewCellContainerMargin * 2.0f + PhotosTableViewCellContentMargin * 2.0f;
    height += actualWidht;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont systemFontOfSize:[UIFont systemFontSize]],
                                  NSParagraphStyleAttributeName : paragraphStyle};

    NSString *likesString = [NSString stringWithFormat:@"%@ ♡", item.likesCount];
    CGSize likesSize = [likesString boundingRectWithSize:CGSizeMake(actualWidht, CGFLOAT_MAX)
                                                 options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                              attributes:attributes
                                                 context:nil].size;
    
    CGSize detailSize = [item.detail boundingRectWithSize:CGSizeMake(actualWidht - PhotosTableViewCellContentOffset - likesSize.width, CGFLOAT_MAX)
                                                  options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                               attributes:attributes
                                                  context:nil].size;
    height += MAX(likesSize.height, detailSize.height) + PhotosTableViewCellContentOffset;
    return height;
}

- (instancetype)init {
    return [self initWithReuseIdentifier:[PhotosTableViewCell reuseIdentifier]];
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _containerInsets = UIEdgeInsetsMake(PhotosTableViewCellContainerMargin,
                                            PhotosTableViewCellContainerMargin,
                                            PhotosTableViewCellContainerMargin,
                                            PhotosTableViewCellContainerMargin);
        
        _contentInsets = UIEdgeInsetsMake(PhotosTableViewCellContentMargin,
                                          PhotosTableViewCellContentMargin,
                                          PhotosTableViewCellContentMargin,
                                          PhotosTableViewCellContentMargin);
        
        _containerView = [[UIView alloc] init];
        _containerView.layer.cornerRadius = 5.0f;
        _containerView.backgroundColor = [UIColor colorWithRed:126.0f / 255.0f
                                                         green:189.0f / 255.0f
                                                          blue:145.0f / 255.0f
                                                         alpha:1.0f];
        [self.contentView addSubview:_containerView];
        
        _photoImageView = [[UIImageView alloc] init];
        _photoImageView.backgroundColor = [UIColor lightGrayColor];
        _photoImageView.contentMode = UIViewContentModeScaleAspectFit;
        [_containerView addSubview:_photoImageView];
        
        _descriptionLabel = [[UILabel alloc] init];
        _descriptionLabel.numberOfLines = 0;
        _descriptionLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        [_containerView addSubview:_descriptionLabel];
        
        _likesLabel = [[UILabel alloc] init];
        _likesLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        [_containerView addSubview:_likesLabel];
        
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _indicatorView.hidden = YES;
        [self.photoImageView addSubview:_indicatorView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.containerView.frame = UIEdgeInsetsInsetRect(self.bounds, self.containerInsets);
    
    CGRect actualBounds = UIEdgeInsetsInsetRect(self.containerView.bounds, self.contentInsets);
    self.photoImageView.frame = CGRectMake(actualBounds.origin.x,
                                           actualBounds.origin.y,
                                           actualBounds.size.width,
                                           actualBounds.size.width);
    
    CGSize likesSize = [self.likesLabel sizeThatFits:CGSizeMake(actualBounds.size.width, CGFLOAT_MAX)];
    CGSize descriptionSize = [self.descriptionLabel sizeThatFits:CGSizeMake(actualBounds.size.width - likesSize.width - PhotosTableViewCellContentOffset, CGFLOAT_MAX)];

    self.likesLabel.frame = CGRectMake(actualBounds.origin.x + (actualBounds.size.width - likesSize.width),
                                       CGRectGetMaxY(self.photoImageView.frame) + PhotosTableViewCellContentOffset,
                                       likesSize.width,
                                       likesSize.height);
    
    self.descriptionLabel.frame = CGRectMake(actualBounds.origin.x,
                                             CGRectGetMaxY(self.photoImageView.frame) + PhotosTableViewCellContentOffset,
                                             descriptionSize.width,
                                             descriptionSize.height);
    
    self.indicatorView.center = CGPointMake(self.photoImageView.bounds.size.width / 2.0f,
                                            self.photoImageView.bounds.size.height / 2.0f);
}

- (void)setItem:(Photo *)item {
    self.descriptionLabel.text = item.detail;
    self.likesLabel.text = [NSString stringWithFormat:@"%@ ♡", item.likesCount];
    
    
    self.indicatorView.hidden = NO;
    [self.indicatorView startAnimating];
    
    self.imageDownloadTaskUUID = [[InstagrammService sharedService] loadDataWithURL:item.url
                                                                          completion:^(BOOL success, NSData *data, NSError *error) {        
        UIImage *image = [UIImage imageWithData:data];
        self.photoImageView.image = image;
        
        self.indicatorView.hidden = YES;
        [self.indicatorView stopAnimating];        
    }];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.photoImageView.image = nil;
    
    self.indicatorView.hidden = YES;
    [self.indicatorView stopAnimating];
    
    if (self.imageDownloadTaskUUID) {
        [[InstagrammService sharedService] cancelTaskWithUUID:self.imageDownloadTaskUUID];
        self.imageDownloadTaskUUID = nil;
    }
}

@end
