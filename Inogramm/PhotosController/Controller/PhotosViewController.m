//
//  PhotosViewController.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "PhotosViewController.h"
#import "PhotosTableViewCell.h"

#import "InstagrammService.h"
#import "Photo.h"

#import "AppDelegate.h"
#import "PhotosModel.h"

@interface PhotosViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, PhotosModelDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) PhotosModel *model;

@end

@implementation PhotosViewController

- (void)loadView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.view = self.tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[PhotosModel alloc] init];
    self.model.delegate = self;
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.navigationItem.hidesBackButton = YES;

    [self.model reloadDataWithCompletion:^{
        [self.tableView reloadData];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.model numberOfRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PhotosTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[PhotosTableViewCell reuseIdentifier]];
    if (!cell) {
        cell = [[PhotosTableViewCell alloc] init];
    }
    [cell setItem:[self.model itemForRow:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [PhotosTableViewCell heightForItem:[self.model itemForRow:indexPath.row] widht:tableView.bounds.size.width];
}

#pragma mark - PhotosModelDelegate

- (void)modelDidUpdate {
    [self.tableView reloadData];
}


@end
