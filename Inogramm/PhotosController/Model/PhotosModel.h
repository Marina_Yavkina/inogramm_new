//
//  PhotosModel.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Photo;

@protocol PhotosModelDelegate <NSObject>

- (void)modelDidUpdate;

@end

@interface PhotosModel : NSObject

@property (nonatomic, weak) id<PhotosModelDelegate> delegate;

- (void)reloadDataWithCompletion:(void (^)())completion;

- (BOOL)canLoadNextPart;
- (void)loadNextPartWithCompletion:(void (^)())completion;

- (NSInteger)numberOfRows;

- (Photo *)itemForRow:(NSInteger)row;

@end
