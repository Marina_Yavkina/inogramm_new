//
//  LoginViewController.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 02/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginView.h"

#import "PushAnimator.h"
#import "PopAnimator.h"

#import "InstagrammLoginViewController.h"

#import "PhotosViewController.h"

#import "InstagrammService.h"

@interface LoginViewController () <UINavigationControllerDelegate, InstagrammLoginViewControllerDelegate>

@property (nonatomic, strong) LoginView *loginView;

@property (nonatomic, strong) InstagrammLoginViewController *instagrammLoginController;

@property (nonatomic, strong) PushAnimator  *pushAnimator;
@property (nonatomic, strong) PopAnimator *popAnimator;

@end

@implementation LoginViewController

- (void)loadView {
    self.loginView = [[LoginView alloc] init];
    self.view = self.loginView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pushAnimator = [[PushAnimator alloc] init];
    self.popAnimator = [[PopAnimator alloc] init];
    
    self.navigationController.delegate = self;
    [self.loginView.instagrammLoginButton addTarget:self action:@selector(loginToInstagramm:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[InstagrammService sharedService] isLoggedIn]) {
        PhotosViewController *vc = [[PhotosViewController alloc] init];
        [self.navigationController pushViewController:vc animated:NO];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)loginToInstagramm:(id)sender {
    self.instagrammLoginController= [[InstagrammLoginViewController alloc] init];
    self.instagrammLoginController.delegate = self;
    [self.instagrammLoginController loadWebConttent];
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
    if (operation == UINavigationControllerOperationPop) {
        return _popAnimator;
    }
    else {
        return _pushAnimator;
    }
}

- (void)didLoadWebContent {
    [self.navigationController pushViewController:self.instagrammLoginController animated:YES];
}

- (void)didRecieveToken:(NSString *)token {
    [self.navigationController popViewControllerAnimated:YES];
    
    [[InstagrammService sharedService] loginWithToken:token];
    
    PhotosViewController *vc = [[PhotosViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
