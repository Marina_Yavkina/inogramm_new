//
//  MappingObject.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 07/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MappingObject <NSObject>

@required
+ (NSDictionary *)mapping;

@optional
+ (NSString *)entityName;
+ (NSString *)primaryKey;

@end
