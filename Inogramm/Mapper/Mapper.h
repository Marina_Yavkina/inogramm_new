//
//  Mapper.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 07/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MappingObject.h"

typedef void (^MapperCompletion) (NSArray *objects);


@interface Mapper : NSObject

+ (NSArray *)objectsOfClass:(Class<MappingObject>)objectClass
               withJSONData:(NSData *)data
                    keypath:(NSString *)keypath;

+ (NSArray *)entitiesOfClass:(Class<MappingObject>)etityClass
                withJSONData:(NSData *)data
                     keypath:(NSString *)keypath
                   inContext:(NSManagedObjectContext *)context;
@end
