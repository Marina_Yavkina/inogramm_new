//
//  Comment.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 07/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "MappingObject.h"

@class Photo;

@interface Comment : NSManagedObject <MappingObject>

@end

#import "Comment+CoreDataProperties.h"
