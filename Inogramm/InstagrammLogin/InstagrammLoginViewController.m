//
//  InstagrammLoginViewController.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 03/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "InstagrammLoginViewController.h"
#import <WebKit/WebKit.h>

@interface InstagrammLoginViewController () <WKNavigationDelegate>

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic ,strong) WKNavigation *loginNavigation;

@end

@implementation InstagrammLoginViewController

- (void)loadView {
    self.view = self.webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if (navigation == self.loginNavigation) {
        if (self.delegate) {
            [self.delegate didLoadWebContent];
        }
    }
}

- (void)loadWebConttent {
    self.webView = [[WKWebView alloc] initWithFrame:CGRectZero
                                      configuration:[[WKWebViewConfiguration alloc] init]];
    self.webView.navigationDelegate = self;
    self.webView.opaque = NO;
    
    NSURL *url = [NSURL URLWithString:@"https://api.instagram.com/oauth/authorize/?client_id=316a9ee569434ec7814d819561b81852&redirect_uri=http://nonactivesite.no&response_type=token"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.loginNavigation = [self.webView loadRequest:request];
}

- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
    NSURL *url = [webView URL];
    NSString *absoluteString = [url absoluteString];
    if ([absoluteString hasPrefix:@"http://nonactivesite.no"]) {
        if (self.delegate) {
            [self.delegate didRecieveToken:[[[url fragment] componentsSeparatedByString:@"="] objectAtIndex:1]];
        }
    }
}

@end
