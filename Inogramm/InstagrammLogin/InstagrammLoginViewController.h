//
//  InstagrammLoginViewController.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 03/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InstagrammLoginViewControllerDelegate <NSObject>

- (void)didLoadWebContent;
- (void)didRecieveToken:(NSString *)token;

@end

@interface InstagrammLoginViewController : UIViewController

@property (nonatomic, weak) id<InstagrammLoginViewControllerDelegate> delegate;

- (void)loadWebConttent;

@end
